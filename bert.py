import torch
from pytorch_pretrained_bert import BertTokenizer, BertModel


class BertEmbedding(object):

    def __init__(self, out_layers=4, device="cpu"):

        # Load pre-trained model (weights)
        self.model = BertModel.from_pretrained('bert-base-uncased')

        self.out_layers = out_layers
        self.out_dims = self.model.config.hidden_size * self.out_layers
        self.device = device

        # Put the model in "evaluation" mode, meaning feed-forward operation.
        self.model.eval()
        self.model.to(device)

    def embedding(self, indexed_tokens):
        # Convert inputs to PyTorch tensors
        tokens_tensor = torch.tensor([indexed_tokens])
        segments_tensors = torch.tensor([1] * len(indexed_tokens))

        # Predict hidden states features for each layer
        with torch.no_grad():
            tokens_tensor = tokens_tensor.to(self.device)
            segments_tensors = segments_tensors.to(self.device)
            encoded_layers, _ = self.model(tokens_tensor, segments_tensors)

        token_embeddings = torch.stack(encoded_layers, dim=0)
        token_embeddings = torch.squeeze(token_embeddings, dim=1)
        token_embeddings = token_embeddings.permute(1, 0, 2)

        # Stores the token vectors
        token_vecs_cat = []

        for token in token_embeddings:
            # Concatenate the vectors (that is, append them together) from the last
            # four layers.
            # Each layer vector is 768 values, so `cat_vec` is length 3,072.
            cat_vec = torch.cat([token[-i] for i in range(1, self.out_layers+1)], dim=0)

            # Use `cat_vec` to represent `token`.
            token_vecs_cat.append(cat_vec)

        return token_vecs_cat


if __name__ == "__main__":

    # Load pre-trained model tokenizer (vocabulary)
    tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')

    text = "Here is the sentence I want embeddings for."

    # Tokenize our sentence with the BERT tokenizer.
    tokenized_text = ["[CLS]"] + tokenizer.tokenize(text) + ["[SEP]"]

    # Print out the tokens.
    print(tokenized_text)

    # Map the token strings to their vocabulary indeces.
    indexed_tokens = tokenizer.convert_tokens_to_ids(tokenized_text)

    be = BertEmbedding()
    embedding_vecs = be.embedding(indexed_tokens)

    for vec in embedding_vecs:
        print(vec)