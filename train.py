#!/usr/bin/env python
import os
import sys
import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np

from datetime import datetime
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm
from bert import BertEmbedding
from dataio.dataset import TokenDataset
from net import BinaryClassifier

CHECKPOINTS_PATH = r"./checkpoints"
TENSORBOARD_PATH = r"./logs/scalars"
SAVE_EACH_NTH_EPOCH = 10000
EPS = 1.0e-8


def f_measure(p, r, beta):
    return (1 + beta**2) * p * r / (beta**2 * p + r + EPS)


def evaluation_metrics(tp, tn, fp, fn, f=0.5):
    prec = tp / (tp + fp + EPS)
    recall = tp / (tp + fn + EPS)
    f_m = f_measure(prec, recall, f)
    return {
        "fp": fp,
        "fn": fn,
        "tp": tp,
        "tn": tn,
        "prec": prec,
        "recall": recall,
        "f0.5": f_m,
    }


def cumulative_metrics(pred, gt, threshold=0.5):
    pred_ = (pred > threshold).int()
    gt_ = gt.int()

    tp = torch.sum((pred_ == 1).int() * (gt_ == 1).int())
    tn = torch.sum((pred_ == 0).int() * (gt_ == 1).int())
    fp = torch.sum((pred_ == 1).int() * (gt_ == 0).int())
    fn = torch.sum((pred_ == 0).int() * (gt_ == 0).int())

    return np.array([
        tp.item(),
        tn.item(),
        fp.item(),
        fn.item()
    ])


def train_epoch(train_loader, net, criterion, optimizer, epoch):

    net.train()
    progress_bar = tqdm(enumerate(train_loader),
                        total=len(train_loader),
                        file=sys.stdout)

    running_loss = 0.0
    for i, data in progress_bar:
        # get the inputs; data is a list of [inputs, labels]
        inputs, labels = data

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

        # print statistics
        progress_bar.set_description('    %4d loss: %.5f' % (i + 1, loss.item()))
        running_loss += loss.item()

    running_loss /= len(train_loader)
    return running_loss


def valid_epoch(valid_loader, net, criterion, epoch):

    net.eval()
    progress_bar = tqdm(enumerate(valid_loader, 0), total=len(valid_loader), file=sys.stdout)

    running_loss = 0.0
    metrics = np.array([0, 0, 0, 0])
    with torch.no_grad():
        for i, data in progress_bar:
            # get the inputs; data is a list of [inputs, labels]
            inputs, labels = data

            # forward
            outputs = net(inputs)
            loss = criterion(outputs, labels)

            # print statistics
            progress_bar.set_description('    %4d loss: %.5f' % (i + 1, loss.item()))
            running_loss += loss.item()

            metrics += cumulative_metrics(torch.sigmoid(outputs), labels)

    return running_loss / len(valid_loader), evaluation_metrics(*tuple(metrics))


def train():

    bert_embedding = BertEmbedding(device=torch.device("cuda:0" if torch.cuda.is_available() else "cpu"))
    net = BinaryClassifier(bert_embedding.out_dims)

    train_dataloader = TokenDataset(bert_embedding,
                                    "./substituted-words/data/val.src",
                                    "./substituted-words/data/val.lbl",
                                    validation=False,
                                    batch_size=32)

    valid_dataloader = TokenDataset(bert_embedding,
                                    "./substituted-words/data/train_tiny.src",
                                    "./substituted-words/data/train_tiny.lbl",
                                    validation=True,
                                    batch_size=32)

    criterion = nn.BCEWithLogitsLoss()
    optimizer = optim.Adam(net.parameters(), lr=0.001)

    max_valid_f_measure = 0.0
    writer = SummaryWriter(log_dir=os.path.join(TENSORBOARD_PATH, datetime.now().strftime("%Y%m%d-%H%M%S")))

    for epoch in range(300):

        print("Epoch %d:" % epoch)
        # train network
        train_loss = train_epoch(train_dataloader, net, criterion, optimizer, epoch)

        # validate network
        val_loss, val_metrics = valid_epoch(valid_dataloader, net, criterion, epoch)
        if max_valid_f_measure < val_metrics["f0.5"]:
            max_valid_f_measure = val_metrics["f0.5"]
            save_weights = True
        else:
            save_weights = False

        # print info
        info = "    loss: %f, val_loss: %f" % (train_loss, val_loss) + " "
        info += ", ".join(["%s: %.2f" % (k, v) for k, v in val_metrics.items() if k in ["prec", "recall", "f0.5"]])
        print(info)

        writer.add_scalar('Loss/train', train_loss, epoch)
        writer.add_scalar('Loss/valid', val_loss, epoch)
        writer.add_scalar('Precision/valid', val_metrics["prec"], epoch)
        writer.add_scalar('Recall/valid', val_metrics["recall"], epoch)
        writer.add_scalar('F0.5/valid', val_metrics["f0.5"], epoch)

        if epoch % SAVE_EACH_NTH_EPOCH == 0 or save_weights:
            torch.save(net.state_dict(), os.path.join(CHECKPOINTS_PATH, "model_%03d_%.4f.pth" % (epoch, val_metrics["f0.5"])))


if __name__ == "__main__":
    train()
