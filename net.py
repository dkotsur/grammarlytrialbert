import torch
import torch.nn as nn
import torch.nn.functional as F


class BinaryClassifier(nn.Module):

    def __init__(self, input_features_num, units=[500, 500, 200], dropout=0.5):
        super(BinaryClassifier, self).__init__()
        # an affine operation: y = Wx + b

        self.fc1 = nn.Linear(in_features=input_features_num, out_features=units[0])
        self.dp1 = nn.Dropout(dropout)

        self.fc2 = nn.Linear(in_features=units[0], out_features=units[1])
        self.dp2 = nn.Dropout(dropout)

        self.fc3 = nn.Linear(in_features=units[1], out_features=units[2])
        self.dp3 = nn.Dropout(dropout)

        self.fc4 = nn.Linear(in_features=units[2], out_features=1)

    def forward(self, x):
        x = F.relu(self.dp1(self.fc1(x)))
        x = F.relu(self.dp2(self.fc2(x)))
        x = F.relu(self.dp3(self.fc3(x)))
        x = self.fc4(x)
        return x


if __name__ == "__main__":

    cls = BinaryClassifier()


