#!/usr/bin/env python
import torch
import numpy as np
from tqdm import tqdm
from bert import BertEmbedding
from net import BinaryClassifier
from dataio.load import read_sentences
from dataio.dataset import tokenize_wordpiece
from pytorch_pretrained_bert import BertTokenizer


WEIGHTS_PATH = r"./checkpoints/model_298_0.2742.pth"
INPUT_PATH = r"./substituted-words/data/test.src"
OUTPUT_PATH = r"./substituted-words/data/test.lbl"


def predict():

    tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
    bert_embedding = BertEmbedding(device=torch.device("cuda:0" if torch.cuda.is_available() else "cpu"))

    net = BinaryClassifier(bert_embedding.out_dims)
    net.load_state_dict(torch.load(WEIGHTS_PATH))
    net.eval()

    data = read_sentences(INPUT_PATH)
    result_probs = []
    for sentance in tqdm(data):
        tokens = sentance.strip().lower().split()
        dest_tokens, _, dest_indices = tokenize_wordpiece(tokens, [0] * len(tokens), tokenizer.tokenize,
                                                          return_indices=True)

        dest_sentence = ["[CLS]"] + dest_tokens + ["[SEP]"]

        try:
            indexed_tokens = tokenizer.convert_tokens_to_ids(dest_sentence)
            embedding_vecs = bert_embedding.embedding(indexed_tokens)
            embedding_vecs = torch.stack(embedding_vecs)

            output = net(embedding_vecs)
            pred = torch.sigmoid(output)
            probs = np.squeeze(pred.detach().numpy())

            result = np.zeros(len(tokens), dtype=np.float)
            divisor = dict()
            for i, idx in enumerate(dest_indices):
                result[idx] += probs[i]
                divisor[idx] = divisor.get(idx, 0) + 1

            for k, v in divisor.items():
                result[k] /= v

            result_probs.append(result)
        except Exception as e:
            result_probs.append(np.asarray([0.0] * len(tokens)))
            print(e)

    with open(OUTPUT_PATH, "w") as fout:
        for probs in result_probs:
            fout.write(" ".join(["%.2f" % v for v in probs]) + "\n")


if __name__ == "__main__":
    predict()