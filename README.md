Substituted words
=================

Solution description
----------------
This solution is based on neural networks: 

1. Text data is converted to numerical representation using pre-trained BERT (Bidirectional Encoder Representations from Transformers).
BERT allows us also embed the context of the word into the final feature vector.
The last four layers of BERT are concatenated, giving 3072-dimensional feature vectors.

2. Fully-connected binary classifier predicts random words based on the BERT feature vectors. 
The neural network has three hidden layers with 500, 500, and 200 neurons.

Code description
----------------
* To train neural network run Python script: train.py
* To predict probabilities for some file run: predict.py

Future work
----------------
* Fine-tune BERT architecture together with FC-layers.
* Extend neural network architecture to LSTM, Bidirectional LSTM with BERT embeddings;
* Compare results with similar architectures for Word2Vec, GloVe embedding.
* Apply post-processing and simple rules (e.g., check if a word has an upper case at the beginning of a sentence).