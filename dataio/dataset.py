import sys
import torch
import numpy as np

from tqdm import  tqdm
from sklearn.utils import shuffle
from pytorch_pretrained_bert import BertTokenizer, BertModel, BertForMaskedLM


from dataio.load import read_sequences, read_sentences


def tokenize_wordpiece(source_tokens, source_labels, tokenizer, return_indices=False):
    """Tokenize words based on the WordPiece model"""
    dest_tokens, dest_labels, dest_indices = [], [], []
    for index, token in enumerate(source_tokens):
        for t in tokenizer(token):
            dest_tokens.append(t.lower())
            dest_labels.append(source_labels[index])
            dest_indices.append(index)
    if return_indices:
        return dest_tokens, dest_labels, dest_indices
    return dest_tokens, dest_labels


class TokenDataset(object):

    def __init__(self,
                 bert_embeddings,
                 filename_dat,
                 filename_lbl,
                 batch_size=2,
                 shuffle=True,
                 encoding="utf8",
                 validation=False):
        
        super(TokenDataset, self).__init__()

        # Load pre-trained model tokenizer (vocabulary)
        self._tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')

        # Init BERT word embedding generator
        self._embedding_generator = bert_embeddings

        # Init attributes
        self._filename_dat = filename_dat
        self._filename_lbl = filename_lbl
        self._shuffle = shuffle
        self._batch_size = batch_size
        self._validation = validation

        # Init inputs
        self.num_inputs = bert_embeddings.out_dims
        self.num_targets = 1

        # Read data
        self._sentences = read_sentences(filename_dat, encoding)
        self._annotations = list(read_sequences(filename_lbl, encoding))

        # Init data
        self._data = []
        self._label = []
        self._positive_indices = []
        self._negative_indices = []

        # Init token index
        token_index = 0

        # process tokens
        for index, data in tqdm(enumerate(zip(self._sentences, self._annotations)),
                                total=len(self._annotations),
                                file=sys.stdout):

            sent, anno = data
            dest_sentence, dest_annotation = tokenize_wordpiece(sent.strip().lower().split(),
                                                                anno, self._tokenizer.tokenize)
            dest_sentence = ["[CLS]"] + dest_sentence + ["[SEP]"]
            dest_annotation = np.pad(dest_annotation, 1, "constant")

            # Convert to sequence
            dest_sequence = self._tokenizer.convert_tokens_to_ids(dest_sentence)

            try:
                embeddings = self._embedding_generator.embedding(dest_sequence)
            except Exception as e:
                print(e)
                continue

            # Update data
            for anno_index, anno in enumerate(dest_annotation[1:-1]):
                self._data.append(embeddings[anno_index])
                self._label.append([float(anno)])
                if anno == 1:
                    self._positive_indices.append(token_index)
                else:
                    self._negative_indices.append(token_index)
                token_index += 1

        print("    class 0:", len(self._negative_indices))
        print("    class 1:", len(self._positive_indices))

        if self._shuffle:
            self.shuffle()

    def __getitem__(self, index):

        if index >= len(self):
            raise IndexError("The index (%d) is out of range." % index)

        if self._validation:
            start = index * self._batch_size
            stop = (index + 1) * self._batch_size
            return torch.stack(self._data[start:stop]), torch.tensor(self._label[start:stop])

        start = index * self._batch_size // 2
        stop = (index + 1) * self._batch_size // 2
        batch_data, batch_labels = [], []
        for j in range(start // 2, stop // 2):
            batch_data.append(self._data[self._positive_indices[j]])
            batch_labels.append([1.0])
        for j in range(start // 2, stop // 2):
            batch_data.append(self._data[self._negative_indices[j]])
            batch_labels.append([0.0])

        batch_data, batch_labels = shuffle(batch_data, batch_labels)
        if index + 1 == len(self) and self._shuffle:
            self.shuffle()

        return torch.stack(batch_data), torch.tensor(batch_labels)

    def __len__(self):
        if self._validation:
            length_ = len(self._data) // self._batch_size
        else:
            length_ = 2 * min(len(self._positive_indices), len(self._negative_indices)) // self._batch_size
        return length_

    def shuffle(self):
        self._positive_indices = np.random.permutation(self._positive_indices)
        self._negative_indices = np.random.permutation(self._negative_indices)


if __name__ == "__main__":
    pass