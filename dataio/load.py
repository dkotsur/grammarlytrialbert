#!/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------------
#
# Created on : 25.12.2019 $
# by : dkotsur $
#
# --- imports ----------------------------------------------------------------------------------------------------------
import numpy as np


def read_sentences(filename, encoding="utf8"):
    """Returns list of strings"""
    try:
        with open(filename, "r", encoding=encoding) as filein:
            return filein.readlines()
    except IOError:
        return []


def read_sequences(filename, encoding="utf8", dtype=np.int):
    try:
        with open(filename, "r", encoding=encoding) as filein:
            return map(lambda line: np.asarray(line.strip().split(), dtype=dtype), filein.readlines())
    except IOError:
        return []
